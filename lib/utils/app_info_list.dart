List<Map<String, dynamic>> siteList = [
  {
    'image': 'Kendeck.JPG',
    'place': 'kendeck-Dibamba',
    'localite': 'Littoral',
    'titre_foncier': 'Tf 3994/SN',
    'price': 5000,
  },
  {
    'image': 'konjok.JPG',
    'place': 'konjok',
    'localite': 'Littoral',
    'titre_foncier': 'Tf 3906/SN',
    'price': 5000,
  },
  {
    'image': 'logbajeck.JPG',
    'place': 'logbajeck',
    'localite': 'Littoral',
    'titre_foncier': 'Tf 3904/SN',
    'price': 5000,
  },
  {
    'image': 'Piti.JPG',
    'place': 'Piti',
    'localite': 'Littoral',
    'titre_foncier': 'Tf 3722/SN',
    'price': 5000,
  },
];
List<Map<String, dynamic>> ticketList = [
  {
    'from': {'code': "DLA", 'name': "Douala"},
    'to': {'code': "NDK", 'name': "Kendeck"},
    'flying_time': '8h 30M',
    'date': "1 MAY",
    'Depart': "08:00 AM",
    "number": 23
  },
  {
    'from': {'code': "DLA", 'name': "Douala"},
    'to': {'code': "pit", 'name': "piti"},
    'flying_time': '8h 30M',
    'date': "1 MAY",
    'Depart': "08:00 AM",
    "number": 23
  },
  {
    'from': {'code': "DLA", 'name': "Douala"},
    'to': {'code': "kjo", 'name': "Konjock"},
    'flying_time': '8h 30M',
    'date': "1 MAY",
    'Depart': "08:00 AM",
    "number": 23
  },
  {
    'from': {'code': "DLA", 'name': "Douala"},
    'to': {'code': "loba", 'name': "logbajeck"},
    'flying_time': '8h 30M',
    'date': "1 MAY",
    'Depart': "08:00 AM",
    "number": 23
  },
];
