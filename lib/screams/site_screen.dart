import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:gap/gap.dart';
import 'package:teste/utils/app_layourt.dart';
import 'package:teste/utils/app_styles.dart';

class SiteScreen extends StatelessWidget {
  final Map<String, dynamic> site;
  const SiteScreen({Key? key, required this.site}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    print('prix de la visite ${site['price']}');
    final size = AppLayout.getSize(context);
    return Container(
      width: size.width * 0.6,
      height: AppLayout.getWidth(350),
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 17),
      margin: const EdgeInsets.only(right: 17, top: 5),
      decoration: BoxDecoration(
          color: Styles.primaryColor,
          borderRadius: BorderRadius.circular(24),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.shade200,
              blurRadius: 20,
              spreadRadius: 5,
            )
          ]),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Container(
          height: AppLayout.getHeight(180),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              color: Styles.primaryColor,
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage("assets/images/${site['image']}"))),
        ),
        const Gap(10),
        Text(
          site['place'],
          style: Styles.headLineStyle2.copyWith(color: Styles.kakiColor),
        ),
        const Gap(5),
        Text(
          site['localite'],
          style: Styles.headLineStyle3.copyWith(color: Colors.white),
        ),
        const Gap(8),
        Text(
          site['titre_foncier'],
          style: Styles.headLineStyle1.copyWith(color: Styles.kakiColor),
        ),
        const Gap(5),
        Text(
          '\FCFA${site['price']}/personne',
          style: Styles.headLineStyle4.copyWith(color: Colors.black87),
        ),
      ]),
    );
  }
}
